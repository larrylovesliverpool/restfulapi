﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace RestfulAPI.Controllers
{
    [Route("api/[controller]")]
    public class AuditLogsController : Controller
    {
        // GET: api/AuditLogs
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "Entry A", "Entry B" };
        }

    }
}
